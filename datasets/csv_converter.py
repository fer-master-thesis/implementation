import argparse
import csv
import os


def main():
    parser = argparse.ArgumentParser(description="Text to CSV")
    parser.add_argument("input", type=str, help="Input file path", default=1)
    parser.add_argument("--output", type=str, nargs='?', help="Output CSV path",
                        default="output/output.csv")
    parser.add_argument("--separator", type=str, nargs='?', help="CSV separator",
                        default=",")
    arguments = parser.parse_args()
    input_file = arguments.input
    output_csv = arguments.output
    separator = arguments.separator

    assert os.path.exists(input_file), f"{input_file} doesn't exist"

    with open(output_csv, 'w', newline='') as csv_file:
        with open(input_file, 'r') as file:
            lines = file.readlines()
            header_row = lines[1].split()
            header_row[-1] = header_row[-1].strip()
            header_row.insert(0, 'filename')
            writer = csv.DictWriter(csv_file, fieldnames=header_row, delimiter=separator)
            writer.writeheader()
            rows = []
            for i in range(2, len(lines)):
                row = {}
                line = lines[i].split()
                line[-1] = line[-1].strip()
                for key, value in zip(header_row, line):
                    row[key] = value
                rows.append(row)
            writer.writerows(rows)


if __name__ == '__main__':
    main()
