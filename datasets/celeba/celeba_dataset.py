import argparse
import csv
import os


def parse():
    parser = argparse.ArgumentParser(description="Process CelebA dataset")
    parser.add_argument("input", type=str, help="Input file path", default=1)
    parser.add_argument("--output", type=str, nargs='?', help="Output CSV path",
                        default="output/output.csv")
    arguments = parser.parse_args()
    input_file = arguments.input
    output_csv = arguments.output

    if not os.path.exists(input_file):
        raise Exception("File doesn't exist")

    if not os.path.exists(os.path.dirname(output_csv)):
        os.makedirs(os.path.dirname(output_csv))

    with open(output_csv, 'w', newline='') as csv_file:
        with open(input_file, 'r') as input_file:
            lines = input_file.readlines()
            header_row = lines[1].split()
            header_row[-1] = header_row[-1].strip()
            header_row.insert(0, "Name")
            writer = csv.DictWriter(csv_file, fieldnames=header_row)
            rows = []
            for i in range(2, len(lines)):
                row = {}
                line = lines[i].split()
                line[-1] = line[-1].strip()
                for key, value in zip(header_row, line):
                    row[key] = value
                rows.append(row)
            writer.writerows(rows)


if __name__ == '__main__':
    parse()
