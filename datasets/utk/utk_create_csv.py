import argparse
import csv
import os

races = ['white', 'black', 'asian', 'indian', 'others']
genders = ['male', 'female']
ages = [f"{i}_years" for i in range(0, 121, 10)]


def parse(input_dir: str, output_csv: str):
    """
    Creates CSV file from UTK dataset
    :param input_dir:  images input
    :param output_csv: CSV output
    """

    def parse_class(name: str):
        def parse_age(age: int) -> int:
            return age // 10

        parts = name.split('_')[:-1]
        if len(parts) == 2:
            parts.append('4')
        assert len(parts) == 3, f"{parts} len is {len(parts)}"
        return parse_age(int(parts[0])), int(parts[1]), int(parts[2])

    assert os.path.exists(input_dir) and os.path.isdir(input_dir), f"{input_dir} is not directory"
    with open(output_csv, 'w+', newline='') as output_file:
        headers = ['image_name']
        headers.extend(ages)
        headers.extend(genders)
        headers.extend(races)
        writer = csv.DictWriter(output_file, fieldnames=headers)
        writer.writeheader()
        rows = []
        for name in os.listdir(input_dir):
            parsed_class = parse_class(name)
            row = [name]
            row.extend([1 if i == parsed_class[0] else 0 for i in range(len(ages))])
            row.extend([1 if i == parsed_class[1] else 0 for i in range(len(genders))])
            row.extend([1 if i == parsed_class[2] else 0 for i in range(len(races))])
            rows.append({key: value for key, value in zip(headers, row)})
        writer.writerows(rows)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", type=str, help="Input directory path", default=1)
    parser.add_argument("--output", type=str, nargs='?', help="Output CSV path",
                        default="output.csv")
    arguments = parser.parse_args()
    input_dir = arguments.input
    output_csv = arguments.output
    parse(input_dir, output_csv)
