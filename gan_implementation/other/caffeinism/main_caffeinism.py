import datetime
import os

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable

from gan_implementation.self_implementation.train.util.utk_dataset import CustomUTKDataset


class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.main = nn.Sequential(
            nn.ConvTranspose2d(nz + nfeature, 512, 4, 1, 0, bias=False),
            nn.BatchNorm2d(512),
            nn.ReLU(True),
            nn.ConvTranspose2d(512, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.ReLU(True),
            nn.ConvTranspose2d(256, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.ReLU(True),
            nn.ConvTranspose2d(128, 64, 4, 2, 1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(True),
            nn.ConvTranspose2d(64, nc, 4, 2, 1, bias=False),
            nn.Tanh(),
        )

    def forward(self, x, attr):
        attr = attr.view(-1, nfeature, 1, 1)
        x = torch.cat([x, attr], 1)
        return self.main(x)


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.feature_input = nn.Linear(nfeature, 64 * 64)
        self.main = nn.Sequential(
            nn.Conv2d(nc + 1, 64, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(64, 128, 4, 2, 1, bias=False),
            nn.BatchNorm2d(128),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(128, 256, 4, 2, 1, bias=False),
            nn.BatchNorm2d(256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(256, 512, 4, 2, 1, bias=False),
            nn.BatchNorm2d(512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(512, 1, 4, 1, 0, bias=False),
        )

    def forward(self, x, attr):
        attr = self.feature_input(attr).view(-1, 1, 64, 64)
        x = torch.cat([x, attr], 1)
        return self.main(x).view(-1, 1)


class Trainer:
    def __init__(self):
        self.generator = Generator()
        self.discriminator = Discriminator()
        self.loss = nn.MSELoss()
        self.optimizer_g = optim.Adam(self.generator.parameters(), lr=lr, betas=betas)
        self.optimizer_d = optim.Adam(self.discriminator.parameters(), lr=lr, betas=betas)

        self.generator.cuda()
        self.discriminator.cuda()
        self.loss.cuda()

    def train(self, dataloader, batch_size):
        noise = Variable(torch.FloatTensor(batch_size, nz, 1, 1).cuda())

        for epoch in range(nepoch):
            for i, (data, attr) in enumerate(dataloader, 0):
                # train discriminator
                self.discriminator.zero_grad()

                batch_size = data.size(0)
                with torch.no_grad():
                    noise.resize_(batch_size, nz, 1, 1).normal_(0, 1)

                attr = attr.cuda().float()

                real = data.cuda()
                d_real = self.discriminator(real, attr)

                fake = self.generator(noise, attr)
                d_fake = self.discriminator(fake.detach(), attr)  # not update generator

                d_loss = -(torch.mean(d_real) - torch.mean(d_fake))  # real label
                d_loss.backward(retain_graph=True)
                self.optimizer_d.step()

                # train generator
                self.generator.zero_grad()
                d_fake = self.discriminator(fake, attr)
                g_loss = -torch.mean(d_fake)  # trick the fake into being real
                g_loss.backward()
                self.optimizer_g.step()
                if i == len(dataloader) - 1:
                    print(f"[{epoch + 1}/{nepoch}] Labels: {[(a == 1).nonzero().item() for a in attr]}")
            print("epoch{:03d} d_real: {}, d_fake: {}".format(epoch, d_real.mean(), d_fake.mean()))
            vutils.save_image(fake.data, '{}/result_epoch_{:03d}.png'.format(result_dir, epoch), normalize=True)


import torch.utils.data

dataset = CustomUTKDataset("/home/mihael/Desktop/Fakultet/10. semestar/Diplomski rad/Dataset/UTKFace",
                           "/home/mihael/Desktop/Fakultet/10. semestar/Diplomski rad/Dataset/output.csv",
                           transforms=transforms.Compose([
                               transforms.Resize((64, 64)),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                           ]), embedding=False)

nz = 100
batch_size = 64
nfeature = dataset.number_of_classes
nc = 3
lr = 0.0002
betas = (0.0, 0.99)
nepoch = 200
result_dir = f"results/{datetime.datetime.now()}"
os.makedirs(result_dir, exist_ok=True)
dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True)
trainer = Trainer()
trainer.train(dataloader, batch_size)
