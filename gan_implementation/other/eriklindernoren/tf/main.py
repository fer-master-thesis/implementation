from __future__ import print_function, division

import os
from datetime import datetime

import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.layers import BatchNormalization, Embedding
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, multiply
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Sequential, Model
from keras.optimizers import Adam

from gan_implementation.self_implementation.train.util.tensoflow_dataset import TensorflowDataset


class CGAN():
    def __init__(self):
        # Input shape
        self.img_rows = 64
        self.img_cols = 64
        self.channels = 3
        self.img_shape = (self.img_rows, self.img_cols, self.channels)
        self.latent_dim = 100

        self.dataset = TensorflowDataset("/home/mihael/Desktop/Fakultet/10. semestar/Diplomski rad/Dataset/UTKFace",
                                    "/home/mihael/Desktop/Fakultet/10. semestar/Diplomski rad/Dataset/output.csv")
        self.dataset.x_transformations = [
            lambda x: np.asarray(x),
            lambda x: cv2.resize(x, (64, 64)),
            lambda x: (x.astype(np.float32) - 127.5) / 127.5,
            lambda x: np.expand_dims(x, axis=3),
            lambda x: np.squeeze(x)
        ]
        self.dataset.y_transformations = [
            lambda y: y.reshape(-1, 1)
        ]

        self.num_classes = self.dataset.number_of_classes
        self.generator = self.build_generator()

        optimizer = Adam(0.0002, 0.5)

        # Build and compile the discriminator
        self.discriminator = self.build_discriminator()
        self.discriminator.compile(loss=['binary_crossentropy'],
                                   optimizer=optimizer,
                                   metrics=['accuracy'])

        # The generator takes noise and the target label as input
        # and generates the corresponding digit of that label
        noise = Input(shape=(self.latent_dim,))
        label = Input(shape=(1,))
        img = self.generator([noise, label])

        # For the combined model we will only train the generator
        self.discriminator.trainable = False

        # The discriminator takes generated image as input and determines validity
        # and the label of that image
        valid = self.discriminator([img, label])

        # The combined model  (stacked generator and discriminator)
        # Trains generator to fool discriminator
        self.combined = Model([noise, label], valid)
        self.combined.compile(loss=['binary_crossentropy'],
            optimizer=optimizer)

    def build_generator(self):

        model = Sequential()

        model.add(Dense(256, input_dim=self.latent_dim))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dense(512))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dense(1024))
        model.add(LeakyReLU(alpha=0.2))
        model.add(BatchNormalization(momentum=0.8))
        model.add(Dense(np.prod(self.img_shape), activation='tanh'))
        model.add(Reshape(self.img_shape))

        model.summary()

        noise = Input(shape=(self.latent_dim,))
        label = Input(shape=(1,), dtype='int32')
        label_embedding = Flatten()(Embedding(self.num_classes, self.latent_dim)(label))

        model_input = multiply([noise, label_embedding])
        img = model(model_input)

        return Model([noise, label], img)

    def build_discriminator(self):

        model = Sequential()

        model.add(Dense(512, input_dim=np.prod(self.img_shape)))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dense(512))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.4))
        model.add(Dense(512))
        model.add(LeakyReLU(alpha=0.2))
        model.add(Dropout(0.4))
        model.add(Dense(1, activation='sigmoid'))
        model.summary()

        img = Input(shape=self.img_shape)
        label = Input(shape=(1,), dtype='int32')

        label_embedding = Flatten()(Embedding(self.num_classes, np.prod(self.img_shape))(label))
        flat_img = Flatten()(img)

        model_input = multiply([flat_img, label_embedding])

        validity = model(model_input)

        return Model([img, label], validity)

    def train(self, epochs, batch_size=128, sample_interval=50):
        # Adversarial ground truths

        name = f"{datetime.now()}"

        os.makedirs(f"results/{name}")

        for epoch in range(epochs):

            # ---------------------
            #  Train Discriminator
            # ---------------------
            index = 0
            printed = False
            while index < len(self.dataset):
                # Select a random half batch of images
                end = min(index + batch_size, len(self.dataset))
                idx = range(index, end)
                imgs, labels = self.dataset.get_items(idx)

                # Sample noise as generator input
                noise = np.random.normal(0, 1, (len(idx), 100))

                # Generate a half batch of new images
                gen_imgs = self.generator.predict([noise, labels])

                # Train the discriminator
                valid = np.ones((len(idx), 1))
                fake = np.zeros((len(idx), 1))
                d_loss_real = self.discriminator.train_on_batch([imgs, labels], valid)
                d_loss_fake = self.discriminator.train_on_batch([gen_imgs, labels], fake)
                d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

                # ---------------------
                #  Train Generator
                # ---------------------

                # Condition on labels
                sampled_labels = np.random.randint(0, self.num_classes, len(idx)).reshape(-1, 1)

                # Train the generator
                g_loss = self.combined.train_on_batch([noise, sampled_labels], valid)
                if end > len(self.dataset) / 2 and not printed or end == len(self.dataset):
                    print("%d.5 [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (
                    (epoch + 0.5) if not printed else (epoch + 1), d_loss[0], 100 * d_loss[1], g_loss))
                    self.sample_images(epoch, name, sampled_labels)
                    printed = True
                index = end

    def sample_images(self, epoch, name, labels):
        r, c = 2, 5
        noise = np.random.normal(0, 1, (r * c, 100))
        gen_imgs = self.generator.predict([noise, labels])

        # Rescale images 0 - 1
        gen_imgs = 0.5 * gen_imgs + 0.5

        fig, axs = plt.subplots(r, c)
        cnt = 0
        for i in range(r):
            for j in range(c):
                axs[i, j].imshow(gen_imgs[cnt, :, :, 0], cmap='gray')
                axs[i, j].set_title("Digit: %d" % labels[cnt])
                axs[i, j].axis('off')
                cnt += 1
        fig.savefig(f"results/{name}/{epoch}.png")
        plt.close()


if __name__ == '__main__':
    cgan = CGAN()
    cgan.train(epochs=1000, batch_size=32, sample_interval=200)
