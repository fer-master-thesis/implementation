import argparse
import multiprocessing
import os
from datetime import datetime

import matplotlib.pyplot as plt
import torch
import torchvision
import torchvision.datasets as datasets
import torchvision.transforms as transforms
from torch import optim
from torch.utils.tensorboard import SummaryWriter

from gan_implementation.self_implementation.models.discriminator import Discriminator
from gan_implementation.self_implementation.models.generator import Generator
from gan_implementation.self_implementation.models.model_util import initialize_weights, gradient_penalty
from gan_implementation.self_implementation.train.util.utk_dataset import CustomUTKDataset


def train(data: str, labels_input: str, image_size: int = 64, batch_size: int = 64, noise_dimension: int = 100,
          learning_rate: float = 1e-3, channels: int = 3, number_of_epochs: int = 15, output_dir: str = "output",
          critic: int = 5,
          lambda_penalty: float = 10, number_of_classes: int = 5, generator_embedding_size: int = 100,
          dataset_name: str = "utkface", number_of_features: int = 16):
    cpu_count = multiprocessing.cpu_count()
    stat_output = os.path.join(output_dir, "stat")
    checkpoint_output = os.path.join("checkpoint")
    times_path = os.path.join(output_dir, "times")
    generator_results = os.path.join(output_dir, "generatorResults")

    os.makedirs(output_dir, exist_ok=True)
    os.makedirs(os.path.join(checkpoint_output, "losses"), exist_ok=True)
    os.makedirs(generator_results, exist_ok=True)
    os.makedirs(stat_output, exist_ok=True)
    os.makedirs(times_path, exist_ok=True)

    file_name = f"{datetime.now()}"
    os.makedirs(os.path.join(generator_results, file_name))
    with open(os.path.join(times_path, f"times-{file_name}.txt"), "w+") as time_file:
        summary_text = f"Data: {data}, labels_input: {labels_input}, image_size: {image_size}, batch_size: {batch_size}, noise_dimension: {noise_dimension}, learning_rate: {learning_rate}, channels: {channels}, number_of_epochs: {number_of_epochs}, output_dir: {output_dir}, critic: {critic}, lambda_penalty: {lambda_penalty}, number_of_classes: {number_of_classes}, generator_embedding_size: {generator_embedding_size}, dataset_name: {dataset_name}, number_of_features: {number_of_features}"
        print(summary_text)
        time_file.write(summary_text)
        time_file.write(f"\nStarted at {datetime.now()}")
        print(f"Started at {datetime.now()}")
        if dataset_name.upper() == "MNIST":
            print("Using MNIST dataset")
            dataset = datasets.MNIST(root="dataset/", transform=transforms.Compose([
                transforms.Resize((image_size, image_size)),
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]), download=True)
            channels = 1
            number_of_classes = 10
        elif dataset_name.upper() == "UTKFACE" or dataset_name.upper() == "CELEBA":
            dataset = CustomUTKDataset(data, labels_input, transforms=transforms.Compose([
                transforms.Resize((image_size, image_size)),
                transforms.ToTensor(),
                transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
            ]))
            number_of_classes = dataset.number_of_labels()
        number_of_images = len(dataset)
        loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size,
                                             shuffle=True, num_workers=cpu_count)
        device = torch.device("cuda:0")
        print(f"Number of images: {number_of_images}")
        time_file.write(f"\nNumber of images: {number_of_images}")

        generator = Generator(noise_dimension, channels, number_of_features, number_of_classes, image_size,
                              generator_embedding_size).to(device)
        discriminator = Discriminator(channels, image_size, number_of_classes, image_size).to(device)
        initialize_weights(generator)
        initialize_weights(discriminator)

        with open(os.path.join(stat_output, f"stat-model-{file_name}.txt"), 'w+') as f:
            f.write(f"Generator: \n{generator}")
            f.write(f"\nDiscriminator: \n{discriminator}")

        optimizer_generator = optim.Adam(generator.parameters(), lr=learning_rate, betas=(0.0, 0.9))
        optimizer_generator_scheduler = optim.lr_scheduler.StepLR(optimizer_generator, step_size=number_of_epochs // 2,
                                                                  gamma=0.1)
        optimizer_discriminator = optim.Adam(discriminator.parameters(), lr=learning_rate, betas=(0.0, 0.9))
        optimizer_discriminator_scheduler = optim.lr_scheduler.StepLR(optimizer_discriminator,
                                                                      step_size=number_of_epochs // 2, gamma=0.1)

        writer_real = SummaryWriter(f"../logs/real")
        writer_fake = SummaryWriter(f"../logs/fake")
        iter = 0

        generator.train()
        discriminator.train()
        batch_print_size = len(loader) // 10
        real_losses, fake_losses = [], []

        for epoch in range(number_of_epochs):
            def save_image():
                with torch.no_grad():
                    noise_a = torch.randn(number_of_samples, noise_dimension, 1, 1).to(device)
                    fake = generator(noise_a, labels)
                    img_grid_fake = torchvision.utils.make_grid(
                        fake[:32], normalize=True
                    )
                    torchvision.utils.save_image(img_grid_fake,
                                                 os.path.join(generator_results, file_name,
                                                              f"{file_name}_{epoch}.png"))

            for batch_index, (data, labels) in enumerate(loader):
                data = data.to(device)
                number_of_samples = data.shape[0]
                labels = labels.to(device)

                for _ in range(critic):
                    noise = torch.randn(number_of_samples, noise_dimension, 1, 1).to(device)
                    fake = generator(noise, labels)
                    discriminator_real = discriminator(data, labels).reshape(-1)
                    discriminator_fake = discriminator(fake, labels).reshape(-1)
                    grad_penalty = gradient_penalty(discriminator, data, fake, labels, device=device)
                    discriminator_loss = -(
                            torch.mean(discriminator_real) - torch.mean(
                        discriminator_fake)) + lambda_penalty * grad_penalty
                    discriminator.zero_grad()
                    discriminator_loss.backward(retain_graph=True)
                    optimizer_discriminator.step()

                generator_fake = discriminator(fake, labels).reshape(-1)
                fake_loss = -torch.mean(generator_fake)
                generator.zero_grad()
                fake_loss.backward()
                optimizer_generator.step()

                if batch_index % batch_print_size == 0 and batch_index > 0 or batch_index + 1 == len(loader):
                    generator.eval()
                    discriminator.eval()
                    loss_string = f"Epoch [{epoch}/{number_of_epochs}] Batch {batch_index}/{len(loader)} \
                          Loss discriminator: {discriminator_loss:.4f}, loss generator: {fake_loss:.4f}"
                    time_file.write(f"\n{loss_string}")
                    print(
                        loss_string
                    )

                    if batch_index + 1 == len(loader):
                        real_losses.append(discriminator_loss)
                        fake_losses.append(fake_loss)

                    with torch.no_grad():
                        fake = generator(noise, labels)
                        img_grid_real = torchvision.utils.make_grid(
                            data[:32], normalize=True
                        )
                        img_grid_fake = torchvision.utils.make_grid(
                            fake[:32], normalize=True
                        )
                        print(f"[{epoch}/{number_of_epochs} | {batch_index}/{len(loader)}] Labels: {labels}")
                        time_file.write(f"[{epoch}/{number_of_epochs} | {batch_index}/{len(loader)}] Labels: {labels}")
                        writer_real.add_image("Real", img_grid_real, global_step=iter)
                        writer_fake.add_image("Fake", img_grid_fake, global_step=iter)

                    iter += 1
                    generator.train()
                    discriminator.train()
            optimizer_discriminator_scheduler.step()
            optimizer_generator_scheduler.step()
            print(f"Lr: {optimizer_generator_scheduler.get_lr()}")
            save_image()

        time_file.write(f"\nEnded at {datetime.now()}")
        torch.save({
            'discriminator_state_dict': discriminator.state_dict(),
            'generator_state_dict': generator.state_dict(),
            'dis_optimizer_state_dict': optimizer_discriminator.state_dict(),
            'gen_optimizer_state_dict': optimizer_generator.state_dict(),
        }, os.path.join(checkpoint_output, f"{file_name}.txt"))
        print(f"\nEnded at {datetime.now()}")

        plt.figure(figsize=(10, 5))
        plt.title("Loss during training")
        plt.plot(fake_losses, label="Generator")
        plt.plot(real_losses, label="Discriminator")
        plt.xlabel("Iterations")
        plt.ylabel("Loss")
        plt.legend()
        plt.savefig(f"{checkpoint_output}/losses/{file_name}.png")
        plt.show()

        with torch.no_grad():
            noise_a = torch.randn(number_of_samples, noise_dimension, 1, 1).to(device)
            fake = generator(noise_a, labels)
            img_grid_fake = torchvision.utils.make_grid(
                fake[:32], normalize=True
            )
            torchvision.utils.save_image(img_grid_fake, os.path.join(checkpoint_output, f"{file_name}.png"))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", type=str, help="Input image directory path")
    parser.add_argument("labels", type=str, help="Labels input")
    parser.add_argument("--image-size", type=int, help="Image size", default=64, nargs="?")
    parser.add_argument("--batch-size", type=int, help="Batch size", default=64, nargs="?")
    parser.add_argument("--noise-dimension", type=int, help="Noise dimension", default=100, nargs="?")
    parser.add_argument("--learning-rate", type=float, help="Learning rate", default=1e-4, nargs="?")
    parser.add_argument("--channels", type=int, help="Number of channels", default=3, nargs="?")
    parser.add_argument("--epochs", type=int, help="Number of epochs", default=15, nargs="?")
    parser.add_argument("--output", type=str, help="Output dir", default="output", nargs="?")
    parser.add_argument("--critic", type=int, help="Number of critic iterations", default=5, nargs="?")
    parser.add_argument("--lambda-penalty", type=float, help="Lambda for gradient penalty", default=10, nargs="?")
    parser.add_argument("--number-of-classes", type=int, help="Number of classes", default=5, nargs="?")
    parser.add_argument("--embedding-size", type=int, help="Generator embedding size", default=100, nargs="?")
    parser.add_argument("--dataset", type=str, help="Dataset", default="utkface", nargs="?")
    parser.add_argument("--dataset-number-of-features", type=int, help="Number of features", default=16, nargs="?")

    arguments = parser.parse_args()

    train(arguments.input, arguments.labels, arguments.image_size, arguments.batch_size, arguments.noise_dimension,
          arguments.learning_rate,
          arguments.channels, arguments.epochs, arguments.output, arguments.critic, arguments.lambda_penalty,
          arguments.number_of_classes, arguments.embedding_size, arguments.dataset, arguments.dataset_number_of_features)
