import argparse
import csv
import datetime
import os

import pandas as pd

from gan_implementation.self_implementation.train.util.train_util import get_unique_values


def main(input_csv: str, output: str):
    assert os.path.exists(input_csv), f"{input_csv} doesn't exist"
    csv_file = pd.read_csv(input_csv)
    unique_values = get_unique_values(csv_file)
    if not os.path.exists(output) and len(output) != 0:
        os.makedirs(output)
    with open(os.path.join(output, f"{datetime.datetime.now()}.csv"), "w+") as f:
        a = ['index']
        a.extend(csv_file.columns[1:])
        writer = csv.DictWriter(f, fieldnames=a)
        writer.writeheader()
        rows = []
        for index, value in enumerate(unique_values):
            values = [index]
            values.extend([1 if x == 1 else 0 for x in value])
            rows.append({key: value for key, value in zip(a, values)})
        writer.writerows(rows)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input_csv", help="CSV file location", type=str)
    parser.add_argument("--output", type=str, help="Output location", default="", nargs="?")
    parsed_args = parser.parse_args()

    main(parsed_args.input_csv, parsed_args.output)
