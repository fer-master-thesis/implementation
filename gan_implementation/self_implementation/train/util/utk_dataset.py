import os
from typing import Union

import pandas as pd
import torch
from PIL import Image
from torch.utils.data import Dataset
from torchvision.transforms import Compose

from gan_implementation.self_implementation.train.util.train_util import map_multiclass_image_to_singleclass


class CustomUTKDataset(Dataset):
    """
    Custom UTK dataset
    """

    def __init__(self, input_dir: str, input_csv: str, transforms: Union[Compose, None] = None, embedding=True):
        """
        Init
        :param input_dir: pictures directory
        :param input_csv: input labels CSV
        :param transforms: transforms for pictures
        """
        assert os.path.exists(input_dir) and os.path.isdir(input_dir), f"Directory {input_dir} doesn't exist"
        assert os.path.exists(input_csv), f"CSV {input_csv} doesn't exist"
        input_file = pd.read_csv(input_csv)

        self.x, self.y, self.number_of_classes = map_multiclass_image_to_singleclass(input_file)
        if not embedding:
            self.y = [torch.as_tensor([1 if x == i else 0 for x in range(self.number_of_classes)]) for i in self.y]
        self.x = [os.path.join(input_dir, x) for x in self.x]
        self.transforms = transforms

    def __len__(self):
        return len(self.y)

    def __getitem__(self, item):
        def load_image():
            image = Image.open(self.x[item])
            return image if self.transforms is None else self.transforms(image)

        return load_image(), self.y[item]

    def number_of_labels(self):
        """
        Number of unique labels in dataset
        :return: number of unique labels
        """
        return self.number_of_classes
