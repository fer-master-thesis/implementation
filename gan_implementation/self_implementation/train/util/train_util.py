import csv
import datetime
import os
from typing import List

import pandas as pd


def number_of_labels(path: str) -> int:
    assert os.path.exists(path), f"Location {path} doesn't exist"
    pf = pd.read_csv(path)
    total = 1
    for i, (name, item) in enumerate(pf.iteritems()):
        if i != 0:
            total *= len(item.unique())
    return total


def get_unique_values(file) -> List:
    unique_values = set()
    for values in file.iloc:
        unique_values.add(tuple(values[1:]))
    return list(unique_values)


def map_multiclass_image_to_singleclass(csv_file, output=None) -> (List, List):
    """
    Converts multiclass images to single class
    :param output: output of labels csv file
    :param csv_file: csv file
    :return: mapped data with labels and number of classes
    """
    unique_values = get_unique_values(csv_file)
    x, y = [], []

    if output is not None:
        if not os.path.exists(output):
            os.makedirs(output)
        with open(os.path.join(output, f"{datetime.datetime.now()}.csv"), "w+") as f:
            a = ['index']
            a.extend(csv_file.columns[1:])
            writer = csv.DictWriter(f, fieldnames=a)
            writer.writeheader()
            rows = []
            for index, value in enumerate(unique_values):
                values = [index]
                values.extend(value)
                rows.append({key: value for key, value in zip(a, values)})
            writer.writerows(rows)
    for values in csv_file.iloc:
        name = values[0]
        x.append(name)
        index = unique_values.index(tuple(values[1:]))
        assert index >= 0
        y.append(index)
    return x, y, len(unique_values)
