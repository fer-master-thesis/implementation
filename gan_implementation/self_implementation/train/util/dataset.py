from typing import List

import numpy as np
from PIL import Image


class Dataset:
    def __init__(self):
        self.x = []
        self.y = []

    def get_items(self, indexes) -> (List[np.ndarray], List[int]):
        def load_image(item: int):
            image = Image.open(self.x[item])
            return image

        return [load_image(i) for i in indexes], np.array([self.y[i] for i in indexes][:])

    def __apply_transformations(self, x, trans):
        if trans is not None:
            for trans in trans:
                for i in range(len(x)):
                    x[i] = trans(x[i])

    def __len__(self):
        return self.x.__len__()

    def __getitem__(self, item):
        items = self.get_items([item])
        return items[0] if len(items) != 0 else None
