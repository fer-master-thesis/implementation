import os
from typing import List

import numpy as np
import pandas as pd

from gan_implementation.self_implementation.train.util.dataset import Dataset
from gan_implementation.self_implementation.train.util.train_util import map_multiclass_image_to_singleclass


class TensorflowDataset(Dataset):
    def __init__(self, input_dir: str, input_csv: str, x_transformations=None, y_transformations=None):
        super(TensorflowDataset, self).__init__()
        assert os.path.exists(input_dir) and os.path.isdir(input_dir), f"Directory {input_dir} doesn't exist"
        assert os.path.exists(input_csv), f"CSV {input_csv} doesn't exist"
        input_file = pd.read_csv(input_csv)

        self.x, self.y, self.number_of_classes = map_multiclass_image_to_singleclass(input_file)
        self.x = [os.path.join(input_dir, x) for x in self.x]
        self.x_transformations = x_transformations
        self.y_transformations = y_transformations

    def __apply_transformations(self, x, trans):
        if trans is not None:
            for trans in trans:
                for i in range(len(x)):
                    x[i] = trans(x[i])

    def get_items(self, indexes) -> (List[np.ndarray], List[int]):
        x, y = super(TensorflowDataset, self).get_items(indexes)
        self.__apply_transformations(x, self.x_transformations)
        for i in self.y_transformations:
            y = i(y)
        return x, y
