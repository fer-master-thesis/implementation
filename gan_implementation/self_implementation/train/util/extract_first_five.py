import argparse
import csv
import datetime
import os

import numpy as np
import pandas as pd


def main(input_csv: str, count: int, output: str):
    assert os.path.exists(input_csv), f"{input_csv} doesn't exist"
    csv_file = pd.read_csv(input_csv)
    if not os.path.exists(output) and len(output) != 0:
        os.makedirs(output)
    with open(os.path.join(output, f"{datetime.datetime.now()}.csv"), "w+") as f:
        a = ['index']
        a.extend(csv_file.columns[1:1 + count])
        writer = csv.DictWriter(f, fieldnames=a)
        writer.writeheader()
        rows = []
        for value in csv_file.iloc:
            if np.any([True if x == 1 else False for x in value[1:1 + count]]):
                b = value[1:1 + count]
                rows.append({key: value for key, value in zip(a, value[:1 + count])})
        print(f"Number of rows: {len(rows)}")
        writer.writerows(rows)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input_csv", help="CSV file location", type=str)
    parser.add_argument("count", help="Number of fields", type=int)
    parser.add_argument("--output", type=str, help="Output location", default="", nargs="?")
    parsed_args = parser.parse_args()

    main(parsed_args.input_csv, parsed_args.count, parsed_args.output)
