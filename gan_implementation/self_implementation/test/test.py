import argparse
import os
from datetime import datetime

import numpy as np
import pandas as pd
import torch
import torchvision
from PIL import Image

from gan_implementation.self_implementation.models.generator import Generator
from gan_implementation.self_implementation.train.util.train_util import get_unique_values


def test(input_file: str, output_file: str = "output",
         model_location: str = "../train/checkpoint/2021-04-22 09:56:55.526803.txt", number_of_samples: int = 10,
         channels: int = 3,
         noise_dimension: int = 100,
         image_size: int = 64, generator_embedding: int = 100, number_of_features: int = 16, on_gpu=True):
    number_of_classes = len(get_unique_values(pd.read_csv(input_file)))

    device = torch.device("cuda:0")

    model = Generator(noise_dimension, channels, number_of_features, number_of_classes, image_size,
                      generator_embedding)
    if on_gpu:
        model = model.to(device)
    if on_gpu:
        loaded_model = torch.load(model_location)['generator_state_dict']
    else:
        loaded_model = torch.load(model_location, map_location='cpu')['generator_state_dict']
    model.load_state_dict(loaded_model)
    model.eval()

    if not os.path.exists(output_file):
        os.makedirs(output_file)

    with torch.no_grad():
        noise_a = torch.randn(number_of_samples, noise_dimension, 1, 1)
        labels = torch.tensor(np.random.randint(number_of_classes, size=(number_of_samples,)))

        if on_gpu:
            noise_a = noise_a.to(device)
            labels = labels.to(device)

        print(f"Labels are: {labels.cpu().numpy()}")
        fake = model(noise_a, labels)
        img_grid_fake = torchvision.utils.make_grid(
            fake[:32], normalize=True
        )
        path = os.path.join(output_file,
                            f"{datetime.now()}.png")
        torchvision.utils.save_image(img_grid_fake, path)
        return os.path.abspath(path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", type=str, help="Input file")
    parser.add_argument("--output", type=str, default="output", nargs="?", help="Output location")
    parser.add_argument("--model-name", type=str, default="../train/checkpoint/2021-04-22 09:56:55.526803.txt",
                        nargs="?", help="Location of model")
    parser.add_argument("--number-of-samples", type=int, default=10, nargs="?", help="Number of samples")
    parser.add_argument("--channels", type=int, default=3, nargs="?", help="Channels in image")
    parser.add_argument("--image-size", type=int, default=64, nargs="?", help="Image size")
    parser.add_argument("--noise-dimension", type=int, default=100, nargs="?", help="Noise dimension")
    parser.add_argument("--generator-embedding", type=int, default=100, nargs="?", help="Generator embedding size")
    parser.add_argument("--number-of-features", type=int, default=16, nargs="?", help="Number of generator features")
    parser.add_argument("--on-gpu", type=bool, default=True, nargs="?", help="Use GPU")

    arguments = parser.parse_args()
    input_file = arguments.input
    output_location = arguments.output
    number_of_samples = arguments.number_of_samples
    channels = arguments.channels
    noise_dimension = arguments.noise_dimension
    generator_embedding = arguments.generator_embedding
    image_size = arguments.image_size
    model_location = arguments.model_name
    number_of_features = arguments.number_of_features
    on_gpu = arguments.on_gpu

    test(input_file, output_location, model_location, number_of_samples, channels, noise_dimension, image_size,
         generator_embedding, number_of_features, on_gpu)
