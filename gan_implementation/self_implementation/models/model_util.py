import torch
import torch.nn as nn


def initialize_weights(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)


def gradient_penalty(critic, real_image, fake_image, labels, device="cpu"):
    batch_size, channels, height, width = real_image.shape
    epsilon = torch.rand((batch_size, 1, 1, 1)).repeat(1, channels, height, width).to(device)
    interpolated_image = real_image * epsilon + fake_image * (1 - epsilon)

    mixed_scores = critic(interpolated_image, labels)
    gradient = \
        torch.autograd.grad(inputs=interpolated_image, outputs=mixed_scores,
                            grad_outputs=torch.ones_like(mixed_scores),
                            create_graph=True, retain_graph=True)[0]
    gradient = gradient.view(gradient.shape[0], -1)
    gradient_norm = gradient.norm(2, dim=1)
    penalty = torch.mean((gradient_norm - 1) ** 2)
    return penalty
