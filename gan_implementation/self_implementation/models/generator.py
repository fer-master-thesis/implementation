import torch
import torch.nn as nn


class Generator(nn.Module):
    def __init__(self, channels_noise, channels_img, features_g, number_of_classes, image_size, embedding_size):
        super(Generator, self).__init__()
        self.image_size = image_size
        self.embedding = nn.Embedding(number_of_classes, embedding_size)

        def convolutional_layer(input_dim, output_dim, kernel_size=4, stride=2, padding=1):
            return nn.Sequential(
                nn.ConvTranspose2d(
                    input_dim,
                    output_dim,
                    kernel_size,
                    stride,
                    padding,
                    bias=False,
                ),
                nn.BatchNorm2d(output_dim),
                nn.ReLU(),
            )

        self.generator = nn.Sequential(
            convolutional_layer(channels_noise + embedding_size, features_g * 16, 4, 1, 0),
            convolutional_layer(features_g * 16, features_g * 8),
            convolutional_layer(features_g * 8, features_g * 4),
            convolutional_layer(features_g * 4, features_g * 2),
            nn.ConvTranspose2d(
                features_g * 2, channels_img, kernel_size=4, stride=2, padding=1
            ),
            nn.Tanh(),
        )

    def forward(self, x, labels):
        embedding = self.embedding(labels).unsqueeze(2).unsqueeze(3)
        x = torch.cat([x, embedding], dim=1)
        return self.generator(x)
