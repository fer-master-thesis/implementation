import torch
import torch.nn as nn


class Discriminator(nn.Module):
    def __init__(self, channels_img, features_d, number_of_classes, image_size=64):
        super(Discriminator, self).__init__()

        def convolutional_layer(input_dim, output_dim, kernel_size=4, stride=2, padding=1):
            return nn.Sequential(
                nn.Conv2d(
                    input_dim,
                    output_dim,
                    kernel_size,
                    stride,
                    padding,
                    bias=False,
                ),
                nn.InstanceNorm2d(output_dim, affine=True),
                nn.LeakyReLU(0.2),
            )

        self.discriminator = nn.Sequential(
            nn.Conv2d(
                channels_img + 1, features_d, kernel_size=4, stride=2, padding=1
            ),
            nn.LeakyReLU(0.2),
            convolutional_layer(features_d, features_d * 2),
            convolutional_layer(features_d * 2, features_d * 4),
            convolutional_layer(features_d * 4, features_d * 8),
            nn.Conv2d(features_d * 8, 1, kernel_size=4, stride=2, padding=0),
        )
        self.embedding = nn.Embedding(number_of_classes, image_size * image_size)
        self.image_size = image_size

    def forward(self, x, labels):
        embedding = self.embedding(labels).view(labels.shape[0], 1, self.image_size, self.image_size)
        x = torch.cat([x, embedding], dim=1)
        return self.discriminator(x)
