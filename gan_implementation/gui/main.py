import os
import tkinter as tk
import tkinter.filedialog as filedialog
import tkinter.messagebox
from tkinter import ttk

from PIL import ImageTk, Image

from gan_implementation.self_implementation.test import test
from gan_implementation.self_implementation.train.train import train


def check_if_exists(*args):
    for x in args:
        if not os.path.exists(x):
            return False
    return True


def insert_value(field, text):
    field.delete(0, tk.END)
    field.insert(0, text)


def get_dir(dir_field):
    filename = filedialog.askdirectory()
    if len(filename) != 0:
        insert_value(dir_field, filename)


def get_file(file_field):
    filename = filedialog.askopenfilename()
    if len(filename) != 0:
        insert_value(file_field, filename)


def create_grid_component(root_frame, row: int, column: int, label_text: str, default_value: str):
    label = tk.Label(root_frame, text=label_text)
    label.grid(row=row, column=column)
    text = tk.Entry(root_frame)
    text.grid(row=row, column=column + 1)
    insert_value(text, default_value)

    return text


def create_chooser_component(root_frame, row: int, label_text: str, button_text: str, is_dir: bool, column: int = 0,
                             default_value: str = ""):
    label = tk.Label(root_frame, text=label_text)
    label.grid(row=row, column=column)
    field = tk.Entry(root_frame)
    field.grid(row=row, column=column + 1)
    insert_value(field, default_value)
    button = tk.Button(root_frame, text=button_text, fg="black",
                       command=lambda: get_dir(field) if is_dir else get_file(field))
    button.grid(row=row, column=column + 2, padx=(50, 0))

    return field


def create_test_frame(root_frame):
    def start_test():
        if not check_if_exists(model_text.get(), output_text.get(), labels_text.get()) or not os.path.isdir(
                output_text.get()):
            tkinter.messagebox.showerror("Error", "Cannot start testing. Check paths if exist")
            return
        output_dir = output_text.get()
        model_location = model_text.get()
        image_path = test.test(
            labels_text.get(),
            output_dir,
            model_location,
            int(samples_text.get()),
            int(channels_text.get()),
            int(noise_text.get()),
            int(image_text.get()),
            int(embedding_text.get()),
            int(features_text.get()),
            on_gpu=False
        )
        frame = tk.Tk()
        frame.title("Test result")
        img = ImageTk.PhotoImage(Image.open(image_path), master=frame)
        label = tk.Label(frame, image=img)
        label.image = img
        label.pack()
        frame.mainloop()
        print("Done")

    model_text = create_chooser_component(root_frame, 0, "Model path:", "Find model", False)
    labels_text = create_chooser_component(root_frame, 1, "Labels path:", "Find labels", False)
    output_text = create_chooser_component(root_frame, 2, "Output path:", "Find output", True)
    samples_text = create_grid_component(root_frame, 3, 0, "Samples:", "10")
    channels_text = create_grid_component(root_frame, 3, 2, "Channels:", "3")
    noise_text = create_grid_component(root_frame, 4, 0, "Noise dimension:", "100")
    image_text = create_grid_component(root_frame, 4, 2, "Image size:", "64")
    embedding_text = create_grid_component(root_frame, 5, 0, "Embedding size:", "100")
    features_text = create_grid_component(root_frame, 5, 2, "Features size:", "16")

    load_button = tk.Button(root_frame, text="Test", command=start_test)
    load_button.grid(row=6)


def create_train_frame(root_frame):
    def start_train():
        if not check_if_exists(image_csv_text.get(), image_dir_text.get()) or not os.path.isdir(
                image_dir_text.get()):
            tkinter.messagebox.showerror("Error", "Cannot start training. Check CSV file location or image dir")
            return
        print("Starting training")
        train(
            image_dir_text.get(), image_csv_text.get(),
            int(image_text.get()), int(batch_size.get()),
            int(noise_text.get()), float(learning_rate_text.get()),
            int(channels_text.get()), int(epochs_text.get()),
            output_dir.get(), int(critic_text.get()),
            float(lambda_penalty.get()), int(no_of_classes_text.get()),
            int(embedding_size_text.get()), dataset_text.get(),
            int(no_of_features_text.get())
        )
        print("Done")

    image_dir_text = create_chooser_component(root_frame, 0, "Images path:", "Find images", True)
    image_csv_text = create_chooser_component(root_frame, 1, "Csv name:", "Find CSV", False)
    output_dir = create_chooser_component(root_frame, 2, "Output:", "Select output", True, default_value="output")
    image_text = create_grid_component(root_frame, 3, 0, "Image size:", "64")
    batch_size = create_grid_component(root_frame, 3, 2, "Batch size:", "64")
    noise_text = create_grid_component(root_frame, 4, 0, "Noise dimension:", "100")
    learning_rate_text = create_grid_component(root_frame, 4, 2, "Learning rate:", "0.0004")
    channels_text = create_grid_component(root_frame, 5, 0, "Channels:", "3")
    epochs_text = create_grid_component(root_frame, 5, 2, "Epochs:", "15")
    critic_text = create_grid_component(root_frame, 6, 0, "Critic:", "5")
    lambda_penalty = create_grid_component(root_frame, 6, 2, "Lambda penalty:", "10")
    no_of_classes_text = create_grid_component(root_frame, 7, 0, "Number of classes:", "10")
    embedding_size_text = create_grid_component(root_frame, 7, 2, "Embedding size:", "100")
    dataset_text = create_grid_component(root_frame, 8, 0, "Dataset name:", "utkface")
    no_of_features_text = create_grid_component(root_frame, 8, 2, "Number of features:", "16")

    load_button = tk.Button(root_frame, text="Train", command=start_train)
    load_button.grid(row=9)


def main():
    root = tk.Tk()
    root.geometry("600x500")
    root.title("Mihael Macuka - GAN")

    tab_controller = ttk.Notebook(root)

    train_tab = ttk.Frame(tab_controller)
    test_tab = ttk.Frame(tab_controller)

    tab_controller.add(train_tab, text="Train")
    tab_controller.add(test_tab, text="Test")
    tab_controller.pack(expand=1, fill="both")

    create_train_frame(train_tab)
    create_test_frame(test_tab)

    root.mainloop()


if __name__ == '__main__':
    main()
